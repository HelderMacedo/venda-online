<?php

use Adianti\Database\TRecord;

class JhiUser extends TRecord{

    const TABLENAME = 'jhi_user';
    const PRIMARYKEY = 'id';
    const IDPOLICY = 'serial';

    public function __construct($id = NULL, $callObjectLoad = TRUE)
    {
        parent::__construct($id, $callObjectLoad);
        parent::addAttribute('login');
        parent::addAttribute('nome');
        parent::addAttribute('email');
        parent::addAttribute('password_md5');
        parent::addAttribute('acesso_sist_sorteiocap');
        parent::addAttribute('id_estab_sorteiocap');
        parent::addAttribute('acesso_sist_sorteioesp');
        parent::addAttribute('id_estab_sorteioesp');
    }
}