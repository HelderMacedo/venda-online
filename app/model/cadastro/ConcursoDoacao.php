<?php

use Adianti\Database\TRecord;

class ConcursoDoacao extends TRecord{

    const TABLENAME = 'concurso';
    const PRIMARYKEY = 'id';
    const IDPOLICY = 'serial';

    public function __construct($id = NULL, $callObjectLoad = TRUE)
    {
        parent::__construct($id, $callObjectLoad);
        parent::addAttribute('concurso_id_sorteioesp');
        parent::addAttribute('data_inicio_sorteioesp');
        parent::addAttribute('data_fim_sorteioesp');
        parent::addAttribute('data_sorteioesp');
        parent::addAttribute('qte_premios_sorteioesp');
        parent::addAttribute('premio_01_sorteioesp');
        parent::addAttribute('premio_02_sorteioesp');
        parent::addAttribute('premio_03_sorteioesp');
        parent::addAttribute('premio_04_sorteioesp');
        parent::addAttribute('premio_05_sorteioesp');
        parent::addAttribute('qtd_giros_sorteioesp');
        parent::addAttribute('giros_sorteioesp');
        parent::addAttribute('dupla_chance_sorteioesp');
        parent::addAttribute('valor_bilhete_sorteioesp');
    }
}
