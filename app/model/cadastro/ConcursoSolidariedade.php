<?php

use Adianti\Database\TRecord;

class ConcursoSolidariedade extends TRecord{
    const TABLENAME = 'concurso';
    const PRIMARYKEY = 'id';
    const IDPOLICY = 'serial';

    public function __construct($id = NULL, $callObjectLoad = TRUE)
    {
        parent::__construct($id, $callObjectLoad);
        parent::addAttribute('concurso_id_sorteiocap');
        parent::addAttribute('data_inicio_sorteiocap');
        parent::addAttribute('data_fim_sorteiocap');
        parent::addAttribute('data_sorteiocap');
        parent::addAttribute('qte_premios_sorteiocap');
        parent::addAttribute('premio_01_sorteiocap');
        parent::addAttribute('premio_02_sorteiocap');
        parent::addAttribute('premio_03_sorteiocap');
        parent::addAttribute('premio_04_sorteiocap');
        parent::addAttribute('premio_05_sorteiocap');
        parent::addAttribute('qtd_giros_sorteiocap');
        parent::addAttribute('giros_sorteiocap');
        parent::addAttribute('dupla_chance_sorteiocap');
        parent::addAttribute('valor_bilhete_sorteiocap');
    }
}