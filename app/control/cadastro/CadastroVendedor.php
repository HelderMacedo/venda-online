<?php

use Adianti\Control\TAction;
use Adianti\Widget\Datagrid\TDataGridColumn;

class CadastroVendedor extends TPage
{
    
    protected $form;
    protected $datagrid;
    protected $pageNavigation;

    use Adianti\base\AdiantiStandardListTrait;

    public function __construct()
    {
        parent::__construct();
    
        $this->setDatabase('permission');
        $this->setActiveRecord('JhiUser');
        $this->setDefaultOrder('id', 'asc');
        $this->setLimit(10);
    
        $this->addFilterField('nome', 'like', 'nome');

        $this->form = new BootstrapFormBuilder('form_search_vendedor');
        $this->form->setFormTitle('Vendedor');

        $nome = new TEntry('nome');
    
        $this->form->addFields([ new TLabel('Nome')], [$nome]);

        $nome->setSize('100%');

        $this->form->setData( TSession::getValue(__CLASS__.'_filter_data') );

        $btn = $this->form->addAction(_t('Find'), new TAction([$this, 'onSearch']), 'fa:search');
        $btn->class = 'btn btn-sm btn-primary';
        $this->form->addAction(_t('New'),  new TAction(array('CadastroVendedorForm', 'onEdit')), 'fa:plus green');
    
        $this->datagrid = new BootstrapDatagridWrapper(new TDataGrid);
        $this->datagrid->style = 'width: 100%';

        //$column_id = new TDataGridColumn('id', 'Id', 'center', '10%');
        $column_login = new TDataGridColumn('login', 'Login', 'center', '10%');
        $column_nome = new TDataGridColumn('nome', 'Nome', 'left', '20%');
        //$column_acesso_sist_sorteiocap = new TDataGridColumn('acesso_sist_sorteiocap', 'Acesso Solidariedade', 'center');
        $column_id_estab = new TDataGridColumn('id_estab_sorteiocap', 'ID Solid', 'left');
        //$column_acesso_sis_especial =  new TDataGridColumn('acesso_sist_sorteioesp', 'Doação', 'left');
        $column_id_estab_sorteioesp = new TDataGridColumn('id_estab_sorteioesp', 'ID Doação', 'left');
      

        //$this->datagrid->addColumn($column_id);
        $this->datagrid->addColumn($column_login);
        $this->datagrid->addColumn($column_nome);
        //$this->datagrid->addColumn($column_acesso_sist_sorteiocap);
        $this->datagrid->addColumn($column_id_estab);
        //$this->datagrid->addColumn($column_acesso_sis_especial);
        $this->datagrid->addColumn($column_id_estab_sorteioesp);

        $action1   = new TDataGridAction(['CadastroVendedorForm', 'onEdit'], ['id' => '{id}', 'register_state' => 'false']);
        $action2 = new TDataGridAction([$this, 'onDelete'], ['id'=>'{id}']);
         
        $this->datagrid->addAction($action1, _t('Edit'),   'far:edit blue');
        $this->datagrid->addAction($action2 ,_t('Delete'), 'far:trash-alt red');
        
        $this->datagrid->createModel();

        $this->pageNavigation = new TPageNavigation;
        $this->pageNavigation->setAction(new TAction([$this, 'onReload']));

        $panel = new TPanelGroup('', 'white');
        $panel->add($this->datagrid);
        $panel->addFooter($this->pageNavigation);

        $container = new TVBox;
        $container->style = 'width: 100%';
        // $container->add(new TXMLBreadCrumb('menu.xml', __CLASS__));
        $container->add($this->form);
        $container->add($panel);

        parent::add($container);
    }
}
    