<?php

use Adianti\Control\TPage;

class DoacaoForm extends TPage
{
    protected $form;
    private $doacao;

    use Adianti\Base\AdiantiStandardFormTrait;

    public function __construct()
    {
        parent::__construct();

        $this->setDatabase('permission');
        $this->setActiveRecord('ConcursoDoacao');

        $this->form = new BootstrapFormBuilder('form_concurso_doacao');
        $this->form->setFormTitle('Doação');
        $this->form->setClientValidation(true);

        $id                         = new TEntry('concurso_id_sorteioesp');
        $data_inicio_sorteioesp     = new TDateTime('data_inicio_sorteioesp');
        $data_fim_sorteioesp        = new TDateTime('data_fim_sorteioesp');
        $data_sorteioesp            = new TDate('data_sorteioesp');
        $qte_premios_sorteioesp     = new TEntry('qte_premios_sorteioesp');
        $premio_01_sorteioesp        = new TEntry('premio_01_sorteioesp');
        $premio_02_sorteioesp       = new TEntry('premio_02_sorteioesp');
        $premio_03_sorteioesp       = new TEntry('premio_03_sorteioesp');
        $premio_04_sorteioesp       = new TEntry('premio_04_sorteioesp');
        $premio_05_sorteioesp       = new TEntry('premio_05_sorteioesp');
        $qtd_giros_sorteioesp       = new TEntry('qtd_giros_sorteioesp');
        $giros_sorteioesp           = new TEntry('giros_sorteioesp');
        $dupla_chance_sorteioesp    = new TRadioGroup('dupla_chance_sorteioesp');
        $valor_bilhete_sorteioesp   = new TNumeric('valor_bilhete_sorteioesp', 2, ',', '.', true);
        $bilhete_online             = new TEntry('online');

        $id->setSize('100%');
        $data_inicio_sorteioesp->setSize('50%');
        $data_inicio_sorteioesp->setMask('dd/mm/yyyy hh:ii');
        $data_inicio_sorteioesp->setDatabaseMask('yyyy-mm-dd hh:ii');
        $data_fim_sorteioesp->setSize('50%');
        $data_fim_sorteioesp->setMask('dd/mm/yyyy hh:ii');
        $data_fim_sorteioesp->setDatabaseMask('yyyy-mm-dd hh:ii');
        $data_sorteioesp->setSize('50%');
        $data_sorteioesp->setMask('dd/mm/yyyy');
        $data_sorteioesp->setDatabaseMask('yyyy-mm-dd');
        $qte_premios_sorteioesp->setSize('100%');
        $qte_premios_sorteioesp->setMask('9!');
        $premio_01_sorteioesp->setSize('100%');
        $premio_02_sorteioesp->setSize('100%');
        $premio_03_sorteioesp->setSize('100%');
        $premio_04_sorteioesp->setSize('100%');
        $premio_05_sorteioesp->setSize('100%');
        $qtd_giros_sorteioesp->setSize('100%');
        $qtd_giros_sorteioesp->setMask('9!');
        $giros_sorteioesp->setSize('100%');
        $dupla_chance_sorteioesp->setSize('100%');
        $dupla_chance_sorteioesp->setLayout('horizontal');
        $dupla_chance_sorteioesp->addItems([1 => 'Sim', 0 => 'Não']);
        $valor_bilhete_sorteioesp->setSize('50%');
        $bilhete_online->setSize('100%');
        $bilhete_online->setEditable(false);

       
        try{
            TTransaction::open('permission');
                

                $this->doacao =  ConcursoDoacao::find(1);
                TTransaction::open('doacao');
                    $con = TTransaction::get();
                    
                    $sql = $con->prepare("select count(*) as total from bilhete b where b.concurso_id = :conc and b.online = 1 ");
                    $sql->bindValue(':conc', $this->doacao->concurso_id_sorteioesp);
                    $sql->execute();
                    $resultado = $sql->fetch();
                    $bilhete_online->setValue($resultado['total']);
                TTransaction::close();
            TTransaction::close();    
        }catch(Exception $e){
            new TMessage('error', $e->getMessage());
            TTransaction::rollback();
        }

        $this->form->addFields([new TLabel('Concurso')], [$id]);
        $this->form->addFields([new TLabel('Inicio Sorteio')], [$data_inicio_sorteioesp]);
        $this->form->addFields([new TLabel('Fim Sorteio')], [$data_fim_sorteioesp]);
        $this->form->addFields([new TLabel('Data Sorteiocap')], [$data_sorteioesp]);
        $this->form->addFields([new TLabel('Qtd. de Prêmios')], [$qte_premios_sorteioesp]);
        $this->form->addFields([new TLabel('Prêmio 01')], [$premio_01_sorteioesp]);
        $this->form->addFields([new TLabel('Prêmio 02')], [$premio_02_sorteioesp]);
        $this->form->addFields([new TLabel('Prêmio 03')], [$premio_03_sorteioesp]);
        $this->form->addFields([new TLabel('Prêmio 04')], [$premio_04_sorteioesp]);
        $this->form->addFields([new TLabel('Prêmio 05')], [$premio_05_sorteioesp]);
        $this->form->addFields([new TLabel('Qtd. Giros Sorteiocap')], [$qtd_giros_sorteioesp]);
        $this->form->addFields([new TLabel('Giros Sorteiocap')], [$giros_sorteioesp]);
        $this->form->addFields([new TLabel('Valor Bilhete')], [$valor_bilhete_sorteioesp]);
        $this->form->addFields([new TLabel('Bilhetes online')], [$bilhete_online]);
        $this->form->addFields([new TLabel('Dupla Chance')], [$dupla_chance_sorteioesp]);


        $btn = $this->form->addAction(_t('Save'), new TAction([$this, 'onSave']), 'fa:save');
        $btn->class = 'btn btn-sm btn-primary';
        $btn_conf_bilhete = $this->form->addAction('Configurar Bilhetes', new TAction([$this, 'onInputDialog']), 'far:edit white');
        $btn_conf_bilhete->class = 'btn btn-sm btn-info';

                

        parent::add($this->form);
        $this->onReload();
    }

    public function onSave($param)
    {
        
        try{
            TTransaction::open('permission');
            
                $data = $this->form->getData();
            
                $object = new ConcursoDoacao(1);
                $object->concurso_id_sorteioesp     = $data->concurso_id_sorteioesp;
                $object->data_inicio_sorteioesp     = $data->data_inicio_sorteioesp;
                $object->data_fim_sorteioesp        = $data->data_fim_sorteioesp;
                $object->data_sorteioesp            = $data->data_sorteioesp;
                $object->qte_premios_sorteioesp     = $data->qte_premios_sorteioesp;
                $object->premio_01_sorteioesp       = $data->premio_01_sorteioesp;
                $object->premio_02_sorteioesp       = $data->premio_02_sorteioesp;
                $object->premio_03_sorteioesp       = $data->premio_03_sorteioesp;
                $object->premio_04_sorteioesp       = $data->premio_04_sorteioesp;
                $object->premio_05_sorteioesp       = $data->premio_05_sorteioesp;
                $object->qtd_giros_sorteioesp       = $data->qtd_giros_sorteioesp;
                $object->giros_sorteioesp           = $data->giros_sorteioesp;
                $object->dupla_chance_sorteioesp    = boolval($data->dupla_chance_sorteioesp);
                $object->valor_bilhete_sorteioesp   = $data->valor_bilhete_sorteioesp;
                $object->store();
            TTransaction::close();
            $this->onReload();
        }catch(Exception $e){

        }
        
    }

    public function onReload($param = NULL)
    {

        try {
            TTransaction::open('permission');

            $repository = new TRepository('ConcursoDoacao');
            $result = $repository->load();
            
            //$this->form->setData($result);
            foreach ($result as $r) {
                $object = new ConcursoDoacao;
                $object->concurso_id_sorteioesp     = $r->concurso_id_sorteioesp;
                //TSession::setValue('concurso_id_doacao', $r->concurso_id_sorteiocap);                
                $object->data_inicio_sorteioesp     = (new DateTime($r->data_inicio_sorteioesp))->format('d-m-Y H:i:s');
                $object->data_fim_sorteioesp        = (new DateTime($r->data_fim_sorteioesp))->format('d-m-Y H:i:s');
                $object->data_sorteioesp            = $r->data_sorteioesp;
                $object->qte_premios_sorteioesp     = $r->qte_premios_sorteioesp;
                $object->premio_01_sorteioesp       = $r->premio_01_sorteioesp;
                $object->premio_02_sorteioesp       = $r->premio_02_sorteioesp;
                $object->premio_03_sorteioesp       = $r->premio_03_sorteioesp;
                $object->premio_04_sorteioesp       = $r->premio_04_sorteioesp;
                $object->premio_05_sorteioesp       = $r->premio_05_sorteioesp;
                $object->qtd_giros_sorteioesp       = $r->qtd_giros_sorteioesp;
                $object->giros_sorteioesp           = $r->giros_sorteioesp;
                $object->dupla_chance_sorteioesp    = boolval($r->dupla_chance_sorteioesp);
                $object->valor_bilhete_sorteioesp   = $r->valor_bilhete_sorteioesp;
            }

            
            $this->form->setData($object);
            TTransaction::close();
        } catch (Exception $e) {
            new TMessage('error', $e->getMessage());
            TTransaction::rollback();
        }
    }

    public function onInputDialog($param){
        
        $form2 = new BootstrapFormBuilder('input_form');
        
        $num_inicial    = new TEntry('numero_inicial');
        $num_fim        = new TEntry('numero_fim');

        $num_inicial->setMask('9!');
        $num_fim->setMask('9!');

        $form2->addFields( [ new TLabel('Bilhete Inicical')], [$num_inicial]);
        $form2->addFields( [ new TLabel('Bilhete Final')], [$num_fim]);

        $form2->addAction('Confirmar', new TAction([__CLASS__, 'onConfirm1']), 'fa:save green');

        new TInputDialog("Configurar Bilhetes", $form2);
    }

    public function onConfirm1($param){
        if($param['numero_inicial'] > $param['numero_fim']){
            return new TMessage('error', 'Bilhete inicial não pode ser maior que o bilhete final');
            exit;
        }    
        
        try {
            TTransaction::open('doacao');
                $con = TTransaction::get();

                $sql = $con->prepare("update bilhete b
                set b.online = 1
                where (b.concurso_id = :Numero_Concurso)
                and ((b.numero >= :Bilhete_Inicial) and (b.numero <= :Bilhete_Final))");
                

                $sql->bindValue(':Numero_Concurso', $this->doacao->concurso_id_sorteioesp);
                $sql->bindValue(':Bilhete_Inicial', $param['numero_inicial']);
                $sql->bindValue(':Bilhete_Final', $param['numero_fim']);
                $sql->execute();
            TTransaction::close();
                new TMessage('info', 'Bilhete alterado com sucesso!');
                AdiantiCoreApplication::loadPage('DoacaoForm');
        } catch (Exception $e) {    
            new TMessage('error', $e->getMessage());
            TTransaction::rollback();
        }
    }
}