<?php

use Adianti\Control\TAction;
use Adianti\Control\TPage;
use Adianti\Database\TRepository;
use Adianti\Database\TTransaction;
use Adianti\Registry\TSession;
use Adianti\Widget\Dialog\TMessage;
use Adianti\Widget\Form\TDate;
use Adianti\Widget\Form\TDateTime;
use Adianti\Widget\Form\TEntry;
use Adianti\Widget\Form\TLabel;
use Adianti\Widget\Form\TRadioGroup;
use Adianti\Wrapper\BootstrapFormBuilder;

class SolidariedadeForm extends TPage
{
    protected $form;
    protected $solidariedade;

    use Adianti\Base\AdiantiStandardFormTrait;

    public function __construct()
    {
        parent::__construct();

        $this->setDatabase('permission');
        $this->setActiveRecord('ConcursoSolidariedade');

        $this->form = new BootstrapFormBuilder('form_concurso_soli');
        $this->form->setFormTitle('Solidariedade');
        $this->form->setClientValidation(true);

        $id                         = new TEntry('concurso_id_sorteiocap');
        $data_inicio_sorteiocap     = new TDateTime('data_inicio_sorteiocap');
        $data_fim_sorteiocap        = new TDateTime('data_fim_sorteiocap');
        $data_sorteiocap            = new TDate('data_sorteiocap');
        $qte_premios_sorteiocap     = new TEntry('qte_premios_sorteiocap');
        $premio_01_sorteioca        = new TEntry('premio_01_sorteiocap');
        $premio_02_sorteiocap       = new TEntry('premio_02_sorteiocap');
        $premio_03_sorteiocap       = new TEntry('premio_03_sorteiocap');
        $premio_04_sorteiocap       = new TEntry('premio_04_sorteiocap');
        $premio_05_sorteiocap       = new TEntry('premio_05_sorteiocap');
        $qtd_giros_sorteiocap       = new TEntry('qtd_giros_sorteiocap');
        $giros_sorteiocap           = new TEntry('giros_sorteiocap');
        $dupla_chance_sorteiocap    = new TRadioGroup('dupla_chance_sorteiocap');
        $valor_bilhete_sorteiocap   = new TNumeric('valor_bilhete_sorteiocap', 2, ',', '.', true);
        $bilhete_online             = new TEntry('online');

        $id->setSize('100%');
        $data_inicio_sorteiocap->setSize('50%');
        $data_inicio_sorteiocap->setMask('dd/mm/yyyy hh:ii');
        $data_inicio_sorteiocap->setDatabaseMask('yyyy-mm-dd hh:ii');
        $data_fim_sorteiocap->setSize('50%');
        $data_fim_sorteiocap->setMask('dd/mm/yyyy hh:ii');
        $data_fim_sorteiocap->setDatabaseMask('yyyy-mm-dd hh:ii');
        $data_sorteiocap->setSize('50%');
        $data_sorteiocap->setMask('dd/mm/yyyy');
        $data_sorteiocap->setDatabaseMask('yyyy-mm-dd');
        $qte_premios_sorteiocap->setSize('100%');
        $qte_premios_sorteiocap->setMask('9!');
        $premio_01_sorteioca->setSize('100%');
        $premio_02_sorteiocap->setSize('100%');
        $premio_03_sorteiocap->setSize('100%');
        $premio_04_sorteiocap->setSize('100%');
        $premio_05_sorteiocap->setSize('100%');
        $qtd_giros_sorteiocap->setSize('100%');
        $qtd_giros_sorteiocap->setMask('9!');
        $giros_sorteiocap->setSize('100%');
        $dupla_chance_sorteiocap->setSize('100%');
        $dupla_chance_sorteiocap->setLayout('horizontal');
        $dupla_chance_sorteiocap->addItems([1 => 'Sim', 0 => 'Não']);
        $valor_bilhete_sorteiocap->setSize('50%');
        $bilhete_online->setSize('100%');
        $bilhete_online->setEditable(false);

        try{
            TTransaction::open('permission');
                $this->solidariedade = ConcursoSolidariedade::find(1);
                TTransaction::open('solidariedade');
                    $con = TTransaction::get();                    
                    
                    $sql = $con->prepare("select count(*) as total from bilhete b where b.concurso_id = :conc and b.online = 1 ");
                    $sql->bindValue(':conc', $this->solidariedade->concurso_id_sorteiocap);
                    $sql->execute();
                    $resultado = $sql->fetch();
                    $bilhete_online->setValue($resultado['total']);
                TTransaction::close();
            TTransaction::close();    
        }catch(Exception $e){
            new TMessage('error', $e->getMessage());
            TTransaction::rollback();
        }

        $this->form->addFields([new TLabel('Concurso')], [$id]);
        $this->form->addFields([new TLabel('Inicio Sorteio')], [$data_inicio_sorteiocap]);
        $this->form->addFields([new TLabel('Fim Sorteio')], [$data_fim_sorteiocap]);
        $this->form->addFields([new TLabel('Data Sorteiocap')], [$data_sorteiocap]);
        $this->form->addFields([new TLabel('Qtd. de Prêmios')], [$qte_premios_sorteiocap]);
        $this->form->addFields([new TLabel('Prêmio 01')], [$premio_01_sorteioca]);
        $this->form->addFields([new TLabel('Prêmio 02')], [$premio_02_sorteiocap]);
        $this->form->addFields([new TLabel('Prêmio 03')], [$premio_03_sorteiocap]);
        $this->form->addFields([new TLabel('Prêmio 04')], [$premio_04_sorteiocap]);
        $this->form->addFields([new TLabel('Prêmio 05')], [$premio_05_sorteiocap]);
        $this->form->addFields([new TLabel('Qtd. Giros Sorteiocap')], [$qtd_giros_sorteiocap]);
        $this->form->addFields([new TLabel('Giros Sorteiocap')], [$giros_sorteiocap]);
        $this->form->addFields([new TLabel('Valor Bilhete')], [$valor_bilhete_sorteiocap]);
        $this->form->addFields([new TLabel('Bilhetes online')], [$bilhete_online]);
        $this->form->addFields([new TLabel('Dupla Chance')], [$dupla_chance_sorteiocap]);


        $btn = $this->form->addAction(_t('Save'), new TAction([$this, 'onSave']), 'fa:save');
        $btn->class = 'btn btn-sm btn-primary';
        $btn_conf_bilhete = $this->form->addAction('Configurar Bilhetes', new TAction([$this, 'onInputDialog']), 'far:edit white');
        $btn_conf_bilhete->class = 'btn btn-sm btn-info';

        parent::add($this->form);
        $this->onReload();
    }

    public function onSave($param)
    {
        
        try{
            TTransaction::open('permission');
            
                $data = $this->form->getData();
                $object = new ConcursoSolidariedade(1);
                $object->concurso_id_sorteiocap     = $data->concurso_id_sorteiocap;
                $object->data_inicio_sorteiocap     = $data->data_inicio_sorteiocap;
                $object->data_fim_sorteiocap        = $data->data_fim_sorteiocap;
                $object->data_sorteiocap            = $data->data_sorteiocap;
                $object->qte_premios_sorteiocap     = $data->qte_premios_sorteiocap;
                $object->premio_01_sorteiocap       = $data->premio_01_sorteiocap;
                $object->premio_02_sorteiocap       = $data->premio_02_sorteiocap;
                $object->premio_03_sorteiocap       = $data->premio_03_sorteiocap;
                $object->premio_04_sorteiocap       = $data->premio_04_sorteiocap;
                $object->premio_05_sorteiocap       = $data->premio_05_sorteiocap;
                $object->qtd_giros_sorteiocap       = $data->qtd_giros_sorteiocap;
                $object->giros_sorteiocap           = $data->giros_sorteiocap;
                $object->dupla_chance_sorteiocap    = boolval($data->dupla_chance_sorteiocap);
                $object->valor_bilhete_sorteiocap   = $data->valor_bilhete_sorteiocap;
                $object->store();
            TTransaction::close();
            $this->onReload();
        }catch(Exception $e){
            new TMessage('error', $e->getMessage());
            TTransaction::rollback();
        }
        
    }

    public function onReload($param = NULL)
    {

        try {
            TTransaction::open('permission');

            $repository = new TRepository('ConcursoSolidariedade');
            $result = $repository->load();

            //$this->form->setData($result);
            foreach ($result as $r) {
                $object = new ConcursoSolidariedade;
                $object->concurso_id_sorteiocap     = $r->concurso_id_sorteiocap;
                
                $object->data_inicio_sorteiocap     = (new DateTime($r->data_inicio_sorteiocap))->format('d-m-Y H:i:s');
                $object->data_fim_sorteiocap        = (new DateTime($r->data_fim_sorteiocap))->format('d-m-Y H:i:s');
                $object->data_sorteiocap            = $r->data_sorteiocap;
                $object->qte_premios_sorteiocap     = $r->qte_premios_sorteiocap;
                $object->premio_01_sorteiocap       = $r->premio_01_sorteiocap;
                $object->premio_02_sorteiocap       = $r->premio_02_sorteiocap;
                $object->premio_03_sorteiocap       = $r->premio_03_sorteiocap;
                $object->premio_04_sorteiocap       = $r->premio_04_sorteiocap;
                $object->premio_05_sorteiocap       = $r->premio_05_sorteiocap;
                $object->qtd_giros_sorteiocap       = $r->qtd_giros_sorteiocap;
                $object->giros_sorteiocap           = $r->giros_sorteiocap;
                $object->dupla_chance_sorteiocap    = boolval($r->dupla_chance_sorteiocap);
                $object->valor_bilhete_sorteiocap   = $r->valor_bilhete_sorteiocap;
            }

            $this->form->setData($object);
            TTransaction::close();
        } catch (Exception $e) {
            new TMessage('error', $e->getMessage());
            TTransaction::rollback();
        }
    }
    
    public function onInputDialog($param){

        $form2 = new BootstrapFormBuilder('input_form');
        
        $num_inicial    = new TEntry('numero_inicial');
        $num_fim        = new TEntry('numero_fim');

        $num_inicial->setMask('9!');
        $num_fim->setMask('9!');

        $form2->addFields( [ new TLabel('Bilhete Inicical')], [$num_inicial]);
        $form2->addFields( [ new TLabel('Bilhete Final')], [$num_fim]);

        $form2->addAction('Confirmar', new TAction([__CLASS__, 'onConfirm1']), 'fa:save green');

        new TInputDialog('Configurar Bilhetes', $form2);
    }

    public  function onConfirm1($param){
        if($param['numero_inicial'] > $param['numero_fim']){
            return new TMessage('error', 'Bilhete inicial não pode ser maior que o bilhete final');
            exit;
        }    
        
        try {
            TTransaction::open('solidariedade');
                $con = TTransaction::get();

                $sql = $con->prepare("update bilhete b
                set b.online = 1
                where (b.concurso_id = :Numero_Concurso)
                and ((b.numero >= :Bilhete_Inicial) and (b.numero <= :Bilhete_Final))");

                

                $sql->bindValue(':Numero_Concurso', $this->solidariedade->concurso_id_sorteiocap);
                $sql->bindValue(':Bilhete_Inicial', $param['numero_inicial']);
                $sql->bindValue(':Bilhete_Final', $param['numero_fim']);
                $sql->execute();
            TTransaction::close();
                new TMessage('info', 'Bilhete alterado com sucesso!');
                AdiantiCoreApplication::loadPage('SolidariedadeForm');
        } catch (Exception $e) {    
            new TMessage('error', $e->getMessage());
            TTransaction::rollback();
        }
    }
}
