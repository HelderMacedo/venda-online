<?php

use Adianti\Database\TTransaction;
use Adianti\Widget\Dialog\TMessage;
use Adianti\Widget\Form\TEntry;

class CadastroVendedorForm extends TPage
{

    protected $form;

    use Adianti\Base\AdiantiStandardFormTrait;

    public function __construct()
    {
        parent::__construct();

        parent::setTargetContainer('adianti_right_panel');
        $this->setAfterSaveAction(new TAction(['CadastroVendedor', 'onReload'], ['register_state' => 'true']));

        $this->setDatabase('permission');
        $this->setActiveRecord('JhiUser');

        $this->form = new BootstrapFormBuilder('form_cad_vendedor');
        $this->form->setFormTitle('Cadastro Vendedor');
        $this->form->setClientValidation(true);
        $this->form->setColumnClasses(2, ['col-sm-5 col-lg-4', 'col-sm-7 col-lg-8']);
    
        $id = new TEntry('id');
        $nome = new TEntry('nome');
        $login = new TEntry('login');
        $senha = new TPassword('password_md5');
        $acesso_sist_sorteio = new TRadioGroup('acesso_sist_sorteiocap');
        $id_estab_sorteiocap = new TEntry('id_estab_sorteiocap');
        $acesso_sist_sorteioesp = new TRadioGroup('acesso_sist_sorteioesp');
        $id_estab_sorteioesp = new TEntry('id_estab_sorteioesp');

        $id->setSize('100%');
        $id->setEditable(false);
        $nome->setSize('100%');
        $nome->forceUpperCase();
        $login->setSize('100%');
        $login->forceLowerCase();
        $senha->setSize('100%');
        $acesso_sist_sorteio->setSize('100%');
        $acesso_sist_sorteio->setLayout('horizontal');
        $id_estab_sorteiocap->setSize('100%');
        $id_estab_sorteiocap->setMask('9!');
        $acesso_sist_sorteioesp->setSize('100%');
        $acesso_sist_sorteioesp->setLayout('horizontal');
        $id_estab_sorteioesp->setSize('100%');
        $id_estab_sorteioesp->setMask('9!');
        $acesso_sist_sorteio->addItems( [1 => 'Sim', 0 => 'Não'] );  
        $acesso_sist_sorteioesp->addItems( [1 => 'Sim', 0 => 'Não'] ); 
        
        $this->form->addFields([new TLabel('Id')], [$id]);

        $this->form->addFields([new TLabel('Nome')], [$nome]);
        $this->form->addFields([new TLabel('Login')], [$login],[new TLabel('Senha')],[$senha]);
        $this->form->addFields([new TLabel('Acesso Sistema SorteioCap')], [$acesso_sist_sorteio]);
        $this->form->addFields([new TLabel('Id Esta. SorteioCap:')], [$id_estab_sorteiocap]);
        $this->form->addFields([new TLabel('Acesso Sistema SorteioEsp')], [$acesso_sist_sorteioesp]);
        $this->form->addFields([new TLabel('Id Estab. SorteioEsp:')], [$id_estab_sorteioesp]);

        $nome->addValidation('Nome', new TRequiredValidator);
        //$senha->addValidation('password_md5', new TRequiredValidator);
        $acesso_sist_sorteio->addValidation('acesso_sist_sorteiocap', new TRequiredValidator);
        $id_estab_sorteiocap->addValidation('id_estab_sorteiocap', new TRequiredValidator);
        $acesso_sist_sorteioesp->addValidation('acesso_sist_sorteioesp', new TRequiredValidator);
        $id_estab_sorteioesp->addValidation('id_estab_sorteioesp', new TRequiredValidator);


        $btn = $this->form->addAction(_t('Save'), new TAction([$this, 'onSave']), 'fa:save');
        $btn->class = 'btn btn-sm btn-primary';
        $this->form->addActionLink(_t('New'),  new TAction([$this, 'onEdit']), 'fa:eraser red');

        $this->form->addHeaderActionLink( _t('Close'), new TAction([$this, 'onClose']), 'fa:times red');

        $container = new TVBox;
        $container->style = '100%';

        $container->add($this->form);

        parent::add($container);
        
    }

    public static function onClose($param)
    {
      TScript::create("Template.closeRightPanel()");
    }
    
    public function onEdit($param)
    {
        try {
            if(isset($param['key'])){
                $key = $param['key'];
                TTransaction::open('permission');
                $object = new JhiUser($key);
                $object->password_md5 = '';
                $this->form->setData($object);

                TTransaction::close();
            }else{
                $this->form->clear(TRUE);
            }
        } catch (Exception $e) {
            new TMessage('error', $e->getMessage());
            TTransaction::rollback();
        }
    }

    public function onSave($param){
        try{

            
            TTransaction::open('permission');
                $data = $this->form->getData();
                if(!empty($data->id)){
                    $usuario = new JhiUser($data->id);
                    $usuario->nome = $data->nome;
                    $usuario->login = $data->login;
                    if($data->password_md5 != null){
                        $usuario->password_md5 = md5($data->password_md5);
                    }
                    $usuario->acesso_sist_sorteiocap = (boolval($data->acesso_sist_sorteiocap));
                    $usuario->id_estab_sorteiocap = $data->id_estab_sorteiocap;
                    $usuario->acesso_sist_sorteioesp = (boolval($data->acesso_sist_sorteioesp));
                    $usuario->id_estab_sorteioesp = $data->id_estab_sorteioesp;
                    $usuario->store();                    
                }else{
                    $usuario = new JhiUser($data->id);
                    $usuario->nome = $data->nome;
                    $usuario->login = $data->login;
                    $usuario->password_md5 = md5($data->password_md5);        
                    $usuario->acesso_sist_sorteiocap = (boolval($data->acesso_sist_sorteiocap));
                    $usuario->id_estab_sorteiocap = $data->id_estab_sorteiocap;
                    $usuario->acesso_sist_sorteioesp = (boolval($data->acesso_sist_sorteioesp));
                    $usuario->id_estab_sorteioesp = $data->id_estab_sorteioesp;
                    $usuario->store();
                }
                

            TTransaction::close();
            new TMessage('info', 'Vendedor Salvo Com Sucesso');
            AdiantiCoreApplication::loadPage('CadastroVendedor');

        }catch(Exception $e){
            new TMessage('error', $e->getMessage());
            TTransaction::rollback();
        }
    }

    
    
}
